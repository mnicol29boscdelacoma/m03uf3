package cat.boscdelacoma.m03.uf3.a2;
/**
 * Centre.java      08/02/2021

 Classe que modela les dades d'un centre educatiu.
 *
 * @author Marc Nicolau <mnicolau@boscdelacoma.cat>
 *
 */
class Centre {
    
    //<editor-fold defaultstate="collapsed" desc="DADES QUE GUARDA UN CENTRE">
    // les dades a guardar
    public String codi;
    public String nom;
    public String naturalesa;
    public String titularitat;
    public String telefon;
    public Comarca comarca;
    public String municipi;
    public String email;
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="OPERACIONS D'UN CENTRE">
    public void mostrar() {
        System.out.printf("\nCODI: %s\n", codi);
        System.out.printf("NOM: %s\n", nom);
        System.out.printf("NATURALESA: %s\n", naturalesa);
        System.out.printf("TITULARITAT: %s\n", titularitat);
        System.out.printf("TELEFON: %s\n", telefon);
        System.out.printf("COMARCA: %s\n", comarca);
        System.out.printf("MUNICPI: %s\n", municipi);
        System.out.printf("CORREU-E: %s\n", email);
        System.out.println("--------------------------------------");
    }
    //</editor-fold>
}
