package cat.boscdelacoma.m03.uf3.a2;

import java.util.Scanner;

/**
 * Comarca.java      08/03/2021
 *
 * Classe que modela les dades d'una comarca d'alumnes.
 *
 * @author Marc Nicolau <mnicolau@boscdelacoma.cat>
 *
 */
class Comarca  {
    
    //<editor-fold defaultstate="collapsed" desc="CONSTANTS">
    // Mida, en bytes, que ocupa ID de comarca
    static final int IDCOMARCA = Integer.BYTES;
    // Mida màxima, en caràcters, que ocupa el nom
    static final int NOM = 20;
    // Mida màxima, en bytes, que ocupa un registre de dades d'una comarca dins el fitxer
    // Atenció:: readUTF interpreta que els caràcters només ocupen 1 byte!!
    static final int MIDAREGISTRE = IDCOMARCA + NOM;
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="DADES D'UNA COMARCA">
    public int idComarca;
    public String nom;
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="LLEGIR LES DADES D'UNA COMARCA">
    /**
     * Demana a l'usuari l'entrada de les dades d'una comarca.
     *
     * @return una tupla Comarca que conté el codi i el nom de la comarca que
     *         corresponen amb el codi de comarca que ha entrat l'usuari,
     *         correctament validat.
     */
    public static Comarca llegirComarca(Scanner lector) {
        boolean ok = false;
        int idComarca = 0;
        Comarca comarca = new Comarca();
        
        System.out.print("Codi de la comarca: ");
        comarca.idComarca = Integer.parseInt(lector.nextLine());
        System.out.print("Nom de la comarca: ");
        comarca.nom = lector.nextLine();
        
        return comarca;
    }
    //</editor-fold>
        
    //<editor-fold defaultstate="collapsed" desc="CONVERSIÓ D'UNA COMARCA A STRING">
    /**
     * @return un String amb les dades de la comarca amb la forma [id,nom]
     */
    public String toString() {
        return String.format("[%d,%s]", idComarca, nom.trim());
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="MOSTRAR UNA COMARCA">
    public void mostrar() {
        System.out.printf("\nCODI: %s\n", idComarca);
        System.out.printf("NOM: %s\n", nom.trim());
        System.out.println("--------------------------------------");
    }
    //</editor-fold>
}
