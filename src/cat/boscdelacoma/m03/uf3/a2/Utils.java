/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.boscdelacoma.m03.uf3.a2;

import static cat.boscdelacoma.m03.uf3.a2.GestioCentres.lector;

/**
 *
 * @author Marc Nicolau
 */
public class Utils {

    /**
     * Demana a l'usuari l'entrada d'una cadena. La longitud de la cadena que es
     * retorna serà la dels caràcters introduits per l'usuari.
     *
     * @return un String amb les dades de la cadena llegida i validada
     */
    public static String llegirCadena(String title) {
        System.out.print(title);
        return lector.nextLine();
    }

    /**
     * Demana a l'usuari l'entrada d'una cadena. La longitud de la cadena que es
     * retorna sempre serà igual al valor maxLength
     *
     * @return un String amb les dades de la cadena llegida i validada
     */
    public static String llegirCadena(String title, int maxLength) {
        boolean ok = false;
        String cadena = "";

        do {
            ok = true;
            System.out.print(title);
            cadena = lector.nextLine();
            if (cadena.length() > maxLength) {
                ok = false;
            }
        } while (!ok);
        // fer que la cadena ocupi la quantitat de caràcters desitjada
        cadena = String.format("%-" + maxLength + "s", " ");

        return cadena;
    }

    /**
     * Converteix una cadena a enter, controlant els possibles errors de
     * conversió
     *
     * @param str la cadena a convertir
     * @return l'enter que representa la cadena inicial.
     */
    public static int stringToInt(String str) {
        int i = 0;

        try {
            i = Integer.parseInt(str);
        } catch (NumberFormatException ex) {
        }
        return i;
    }
}
