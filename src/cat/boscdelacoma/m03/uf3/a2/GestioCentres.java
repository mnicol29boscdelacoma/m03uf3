package cat.boscdelacoma.m03.uf3.a2;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 * GestioCentres.java
 *
 * Programa que guarda en un fitxer relatiu les dades d'alumnes: nom i cognoms
 * (50 chars) edat (int) idGrup(short)
 *
 * @author Marc
 */
public class GestioCentres {

    //<editor-fold defaultstate="collapsed" desc="CONSTANTS">
    static final String FITXER_CENTRES = "centres.txt";
    static final String FITXER_COMARQUES = "comarques.dat";
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="GLOBALS">
    // per llegir des de teclat
    static Scanner lector = new Scanner(System.in);
    //</editor-fold>

    /**
     * Programa principal
     *
     * @param args
     */
    public static void main(String[] args) {
        String opcio = "";

        // Obertura del fitxer orientat a byte amb accés relatiu
        try (RandomAccessFile comarquesFile = new RandomAccessFile(FITXER_COMARQUES, "rw")) {

            // repetir mentre l'opció llegida no sigui '0'
            do {
                menu();
                opcio = lector.nextLine();
                switch (opcio) {
                    case "0":
                        break;
                    case "1":
                        afegirComarca(comarquesFile, Comarca.llegirComarca(lector));
                        break;
                    case "2":
                        mostrarFitxerComarques(comarquesFile);
                        break;
                    case "3":
                        mostrarFitxerCentres(comarquesFile);
                        break;
                    default:
                        System.out.println("Opció incorrecta!");
                        break;
                }
            } while (!opcio.equals("0"));

        } catch (FileNotFoundException e) {
            System.out.printf("Error, no s'ha trobat el fitxer :: %s\n", e);

        } catch (IOException e) {
            System.out.printf("Error en operació d'entrada/sortida :: %s\n", e);
        }
    }

    /**
     * Mostra el menú d'opcions del programa
     */
    private static void menu() {
        System.out.println("G E S T I O  D E  C E N T R E S");
        System.out.println("===============================");
        System.out.println("0. Sortir");
        System.out.println("1. Afegir una comarca");
        System.out.println("2. Mostrar totes les comarques");
        System.out.println("3. Mostrar tots els centres educatius");
        System.out.print("\nEscull una opció: ");
    }

    /**
     * Obté la comarca que té idComarca del fitxer relatiu orientat a byte.
     *
     * @param comarquesFile fitxer orientat a byte amb accés relatiu que conté
     * les comarques
     * @param idComarca l'identificador de comarca que pertany a la comarca que
     * es vol obtenir
     * @return la comarca que té per id idComarca, o null si no el troba.
     */
    private static Comarca obtenirComarca(RandomAccessFile comarquesFile, int idComarca) {
        Comarca comarca = null;
        // bloc try-catch-with resources, per a controlar l'obertura i gestió
        // del fitxer. Fent-ho d'aquesta manera, si és produeix alguna situació
        // excepcional, el fitxer és tanca automàticament (si es trobava obert)
        try {
            // TODO: obtenir les dades de la comarca indicada amb el paràmetre
            //       idComarca i generar un objecte Comarca que es retorna
            
            
            
            
        } catch (IOException ex) {
            // error de lectura --> no ha trobat idComarca --> retorna null
            comarca = null;
        }
        return comarca;
    }

    /**
     * Mostrar el contingut del fitxer de comarques.
     *
     * @param comarquesFile el fitxer de comarques, ja obert
     */
    private static void mostrarFitxerComarques(RandomAccessFile comarquesFile) {
        try {
            // TODO: mostrar totes les comarques del fitxer
            // ull: s'ha de recordar que cada comarca és un bloc de 24 bytes que
            //      conté el idComarca i el nom de la comarca. 
            // Revisar tot el codi i mirar si algun subprograma ens pot simplificar
            // el codi.
            
            
            
            
            
            
        } catch (IOException ex) {
            System.out.printf("Error en llegir el fitxer de comarques: %s\n", ex);
        }
    }

    /**
     * Mostrar el contingut del fitxer de centres educatius.
     *
     * @param comarquesFile el fitxer de comarques, ja obert
     */
    private static void mostrarFitxerCentres(RandomAccessFile comarquesFile) {
        try {
            // TODO : mostrar els centres educatius.
            //        ATENCIÓ -> podeu aplicar altres subprogrames d'aquest mateix
            //                   codi font, que us poden simplificar el codi

            
            
            
            
            
        } catch (IOException ex) {
            System.out.printf("Error en llegir el fitxer centres: %s\n", ex);
        }

    }

    /**
     * Afegeix un nou registre amb dades d'una comarca dins el fitxer de
     * comarques que ja es troba obert. La inserció del nou registre es fa pel
     * final.
     *
     * @param comarquesFile el fitxer de comarques (accés relatiu, orientat a
     * byte)
     * @param comarca conté les dades del nou registre que es vol afegir.
     */
    private static void afegirComarca(RandomAccessFile comarquesFile, Comarca comarca) {
        try {
            // TODO : cal comprovar si el fitxer de comarques ja conté la comarca.
            // en cas que ja existeixi, demanar si es vol sobreescriure el registre, en cas
            // contrari, afegir-lo a la posició que correspon al seu ID.

            
            
            
            
            
            
        } catch (IOException ex) {
            System.out.printf("Error en afegir comarca:: %s\n", ex.getMessage());
        }
    }
    
    /**
     * Converteix les dades d'un string que conté informació d'un centre, a un
     * objecte Centre
     *
     * @param lineCentres cadena que conté informació d'un centre
     * @param comarquesFile fitxer de comarques
     * @return objecte que conté la informació del centre
     */
    private static Centre stringToCentre(String lineCentres, RandomAccessFile comarquesFile) {
        Centre centre = new Centre();
        String[] fields = lineCentres.split("[;]");

        centre.codi = fields[0];
        centre.nom = fields[1];
        centre.naturalesa = fields[2];
        centre.titularitat = fields[3];
        centre.telefon = fields[4];
        centre.comarca = obtenirComarca(comarquesFile, Utils.stringToInt(fields[5]) - 1);
        centre.municipi = fields[6];
        centre.email = fields[7];

        return centre;
    }

}
