# Centres educatius

El Departament d’Educació de la Generalitat de Catalunya està a punt d’iniciar el procés de preinscripció escolar pel curs 2021-2022. Entre les moltes dades que ha de gestionar, hi ha la dels centres escolars. Disposa d'un programa a mig fer i vol completar-lo. Aquest programa ha de permetre llistar els centres escolars, llistar les comarques de Catalunya i afegir noves comarques. 

Heu d'implementar el codi en les zones marcades amb   `// TODO`

Entrant en detall, el que es demana és implementar els següents mètodes:

>	`void mostrarFitxerCentres(RandomAccessFile comarquesFile)`

> Aquest mètode mostra el fitxer de centres educatius (fitxer amb accés seqüencial orientat a caràcter). En aquest fitxer no hi ha el nom de la comarca on pertany el centre educatiu: només conté el codi de comarca.  A l’hora de mostrar les dades de cada centre, també cal mostrar a quina comarca pertany (codi i nom de la comarca).
>
> El mètode rep per paràmetre un fitxer orientat a byte amb accés relatiu ja obert que conté les dades de les comarques de Catalunya, per poder fer la relació de dades entre els dos fitxers.

>	`void mostrarFitxerComarques(RandomAccessFile comarquesFile)`

> Aquest mètode mostra el contingut del fitxer de comarques (fitxer amb accés relatiu orientat a byte). 
>
> El mètode rep per paràmetre un fitxer orientat a byte amb accés relatiu ja obert que conté les dades de les comarques de Catalunya.
>
> El format del fitxer és:    `IDCOMARCA (enter, 4 bytes)  NOMCOMARCA (20 caràcters)`

>	`void afegirComarca(RandomAccessFile comarquesFile, Comarca comarca)`

> Mètode que afegeix un nou registre de dades d’una comarca dins un fitxer amb accés relatiu orientat a byte. La inserció del nou registre es fa segons el valor de l’identificador de la comarca. Concretament, la comarca amb identificador 1, ocuparà el registre 0 del fitxer, l’identificador 5 el registre 4, etc….

> El format del fitxer és:    `IDCOMARCA (enter, 4 bytes)  NOMCOMARCA (20 caràcters)`

>	`Comarca obtenirComarca(RandomAccessFile comarquesFile, int idComarca)`

> Mètode que obté un objecte de tipus Comarca des d’un fitxer amb accés relatiu orientat a byte de comarques. L’identificador de comarca indica quin codi de comarca es vol recuperar. Si no es troba la comarca, ha de retornar el valor NULL

> El format del fitxer és:    `IDCOMARCA (enter, 4 bytes)  NOMCOMARCA (20 caràcters)`


## CONSIDERACIONS
- Cal reaprofitar tant com es pugui els subprogrames: cal revisar-los bé.
- No s’han de crear nous subprogrames.
